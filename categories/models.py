from django.db import models
from django.utils.text import slugify


class Category(models.Model):
    title = models.CharField(max_length=50,
                             help_text='Keep the category name short and descriptive.')
    slug = models.SlugField(max_length=60,
                            unique=True)
    parent = models.ForeignKey('self',
                               on_delete=models.SET_NULL,
                               blank=True,
                               null=True,
                               related_name='parent_category')

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.title
