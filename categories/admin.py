from django.contrib import admin

from .models import Category

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'parent')
    list_filter = ('parent',)
    ordering = ('parent', 'title')
    prepopulated_fields = {'slug': ('title',)}
