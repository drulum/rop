from django.urls import path

from . import views
from .views import Homepage

app_name = 'core'

urlpatterns = [
    path('', Homepage.as_view(), name='homepage'),
]
