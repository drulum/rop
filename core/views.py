# from django.shortcuts import render
from django.views.generic import ListView

from posts.models import Post


class Homepage(ListView):
    queryset = Post.objects.filter(is_published=True).order_by('-created')
    template_name = 'core/homepage.html'
