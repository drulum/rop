from django.contrib.auth.models import AbstractUser
# from django.db import models


# Really simple initially. Only putting this in place for future use.
class User(AbstractUser):

    def __str__(self):
        return self.first_name
