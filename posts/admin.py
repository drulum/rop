from django.contrib import admin

from .models import Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'subtitle', 'created', 'category', 'is_published')
    list_filter = ('is_published', 'category')
    ordering = ('is_published', 'created')
    prepopulated_fields = {'slug': ('title',)}
