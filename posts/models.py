from django.db import models
from django.urls import reverse
from django.utils.html import mark_safe
from django.utils.text import slugify
from markdown import markdown

from categories.models import Category


class Post(models.Model):
    title = models.CharField(max_length=200,
                             help_text='Give your post a short, clear, title.')
    subtitle = models.CharField(max_length=220,
                                help_text='Type in a subtitle if the post will benefit from it.',
                                blank=True,
                                null=True)
    slug = models.SlugField(max_length=200,
                            unique_for_month='created')
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    category = models.ForeignKey(Category,
                                 on_delete=models.SET_NULL,
                                 blank=True,
                                 null=True,
                                 related_name='posts')
    summary = models.TextField(blank=True,
                               null=True)
    body = models.TextField()
    is_published = models.BooleanField()

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        return super().save(*args, **kwargs)

    def body_as_markdown(self):
        return mark_safe(markdown(self.body, safe_mode='escape'))

    def get_absolute_url(self):
        return reverse('posts:post_detail',
                       args=[self.created.year,
                             self.created.month,
                             self.slug])

    def __str__(self):
        return self.title
