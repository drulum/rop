from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, ListView

from .models import Post
from categories.models import Category


class PostList(ListView):

    def get_queryset(self):
        self.category = get_object_or_404(Category, title=self.kwargs['category'])
        return Post.objects.filter(category=self.category).filter(is_published=True).order_by('-created')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = self.category
        context['all_posts'] = Post.objects.all().filter(is_published=True).order_by('-created')
        return context


class PostDetail(DetailView):
    model = Post

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['all_posts'] = Post.objects.all().filter(is_published=True).order_by('-created')
        return context
