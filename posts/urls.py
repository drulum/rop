from django.urls import path

from .views import PostDetail, PostList

app_name = 'posts'

urlpatterns = [
    path('<category>/', PostList.as_view(), name='post_list'),
    path('<int:year>/<int:month>/<slug:slug>/', PostDetail.as_view(), name='post_detail'),
]
